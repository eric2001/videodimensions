# VideoDimensions
![VideoDimensions](./screenshot.jpg) 

## Description
VideoDimensions is a module for Gallery 3 which will allow gallery users to manually edit the height and width of a video that they've uploaded.  This can be useful for web servers that don't have ffmpeg, and are thus unable to automatically determine this information.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "videodimensions" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  The height and width may then be modified from a video's Options->Edit menu.

## History
**Version 1.1.1:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 08 August 2011.
>
> Download: [Version 1.1.1](/uploads/7890001d7c20faf8c5029f7c3b41893e/videodimensions111.zip)

**Version 1.1.0:**
> - Updated for Gallery 3 RC-1 compatibility.
> - Released 25 February 2010.
>
> Download: [Version 1.1.0](/uploads/6a596bd93f80136e37ece0ffa845fa56/videodimensions110.zip)

**Version 1.0.1:**
> - Bug fix:  Make sure the current item is actually a video.
> - Released on 10 August 2009
>
> Download: [Version 1.0.1](/uploads/b46e47053cbed70138820c14b0e89c44/videodimensions101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 10 August 2009
>
> Download: [Version 1.0.0](/uploads/1002f13b12cd80e6eb3339639d9c8a3b/videodimensions100.zip)
